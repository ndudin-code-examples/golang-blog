package main

import (
	"blog_app/routing"
	"log"
	"net/http"
	"os"
)

var Routing routing.Routing

func init() {
	Routing = routing.GetRouting()
}

func main() {
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}
	log.Printf("Serving application on %s port", port)
	err := http.ListenAndServe(":"+port, Routing.Router)
	if err != nil {
		log.Fatal(err)
	}
}
