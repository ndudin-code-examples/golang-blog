package admin

import (
	"blog_app/controllers"
	"blog_app/models"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"strings"
)

func GetArticleController() controllers.RecourseControllerInterface {
	return &ArticleRecourseController{Model: models.GetArticleRecourse(), Router: Router, Name: "article"}
}

type ArticleRecourseController struct {
	controllers.RecourseControllerInterface
	Model  models.RecourseInterface
	Router *mux.Router
	Name   string
}

func (controller *ArticleRecourseController) GetRecourseName() string {
	return controller.Name
}
func (controller *ArticleRecourseController) SetRecourse(recourse models.RecourseInterface) {
	controller.Model = recourse
}
func (controller *ArticleRecourseController) GetRecourse() models.RecourseInterface {
	return controller.Model
}

func (controller *ArticleRecourseController) ReadAll() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		responseData := getStartResponseData(w, r)
		admin := controller.GetRecourse()
		admins, err := admin.FindAll(map[string]interface{}{"is_deleted": false})
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "404")
		}
		var items []map[string]interface{}
		for _, v := range admins {
			g, _ := Router.Get("admin_article_read").URL("id", v.GetId().Hex())
			d, _ := Router.Get("admin_article_delete").URL("id", v.GetId().Hex())
			item := map[string]interface{}{"get_url": g.Path, "del_url": d.Path, "user": v}
			items = append(items, item)
		}
		responseData["items"] = items
		createUrl, _ := Router.Get("admin_article_create").URL("id", "create")
		responseData["create_url"] = createUrl.Path
		controllers.ResponseTemplate(w, responseData, "admin/grids/articles")
	}
}
func (controller *ArticleRecourseController) Read() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		responseData := getStartResponseData(w, r)
		admin := controller.GetRecourse()
		vars := mux.Vars(r)
		id, _ := primitive.ObjectIDFromHex(vars["id"])
		admin, err := admin.FindFirst(map[string]interface{}{"_id": id})
		if err != nil {
			ui, _ := Router.Get("admin_article_create").URL()
			responseData["action_url"] = ui.Path
		} else {
			ui, _ := Router.Get("admin_article_update").URL("id", id.Hex())
			responseData["action_url"] = ui.Path
		}
		ui, _ := Router.Get("admin_article_delete").URL("id", id.Hex())
		responseData["delete_url"] = ui.Path
		responseData["item"] = admin
		controllers.ResponseTemplate(w, responseData, "admin/form/article")
	}
}
func (controller *ArticleRecourseController) Create() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		e := r.ParseForm()
		if e != nil {
			log.Fatal(e)
		}
		responseData := getStartResponseData(w, r)
		admin := controller.GetRecourse().(*models.ArticleRecourse)
		admin.Title = strings.Join(r.Form["title"], "")
		admin.Preview = strings.Join(r.Form["preview"], "")
		admin.Body = strings.Join(r.Form["body"], "")
		admin.Uri = strings.Join(r.Form["uri"], "")
		id, err := admin.Create()
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "500")
		}
		ui, _ := Router.Get("admin_admins_get_admin").URL("id", id.Hex())
		http.Redirect(w, r, ui.Path, http.StatusSeeOther)
	}
}
func (controller *ArticleRecourseController) Update() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		responseData := getStartResponseData(w, r)
		vars := mux.Vars(r)
		id, _ := primitive.ObjectIDFromHex(vars["id"])
		adminRecourse := controller.GetRecourse().(*models.ArticleRecourse)
		item, err := adminRecourse.FindFirst(map[string]interface{}{"_id": id})
		admin := item.(*models.ArticleRecourse)
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "404")
		}
		e := r.ParseForm()
		if e != nil {
			log.Fatal(e)
		}
		title := strings.Join(r.Form["title"], "")
		if title != "" {
			admin.Title = title
		}
		preview := strings.Join(r.Form["preview"], "")
		if preview != "" {
			admin.Preview = preview
		}
		body := strings.Join(r.Form["body"], "")
		if body != "" {
			admin.Body = body
		}
		uri := strings.Join(r.Form["uri"], "")
		if uri != "" {
			admin.Uri = uri
		}
		_, err = admin.Update()
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "500")
		}
		ui, err := Router.Get("admin_article_read").URL("id", vars["id"])
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "500")
		}
		http.Redirect(w, r, ui.Path, http.StatusSeeOther)
	}
}
func (controller *ArticleRecourseController) Delete() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		responseData := getStartResponseData(w, r)
		vars := mux.Vars(r)
		id, _ := primitive.ObjectIDFromHex(vars["id"])
		adminRecourse := controller.GetRecourse().(*models.ArticleRecourse)
		admin, err := adminRecourse.FindFirst(map[string]interface{}{"_id": id})
		if err != nil {
			responseData["error"] = err
			controllers.ReturnError(w, responseData, "admin", "500")
		}
		_, _ = admin.Delete()
		ui, _ := Router.Get("admin_article_read_all").URL("id", vars["id"])
		http.Redirect(w, r, ui.Path, http.StatusSeeOther)
	}
}
