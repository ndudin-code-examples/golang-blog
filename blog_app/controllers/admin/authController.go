package admin

import (
	"blog_app/controllers"
	"blog_app/models"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"strings"
)

var LoginPage = func(w http.ResponseWriter, r *http.Request) {
	responseData := map[string]interface{}{}
	controllers.ResponseTemplate(w, responseData, "admin/forms/login")
}

var LoginAction = func(w http.ResponseWriter, r *http.Request) {
	responseData := map[string]interface{}{}
	err := r.ParseForm()
	if err != nil {
		log.Print("parsing error")
		log.Print(err)
		controllers.ResponseTemplate(w, responseData, "admin/forms/login")
	}
	session, err := getSessionStorage(r)
	if err != nil {
		log.Print("get session")
		log.Print(err)
		controllers.ResponseTemplate(w, responseData, "admin/forms/login")
	}
	admin := models.Admin{}
	login := strings.Join(r.Form["login"], "")
	admin, err = admin.FindFirst(map[string]interface{}{"login": login})
	if err != nil {
		log.Print("db find")
		log.Print(err)
		controllers.ResponseTemplate(w, responseData, "admin/forms/login")
	}
	password := strings.Join(r.Form["password"], "")
	err = bcrypt.CompareHashAndPassword([]byte(admin.Password), []byte(password))
	if err != nil {
		controllers.ResponseTemplate(w, responseData, "admin/forms/login")
	}
	session.Values["user_id"] = admin.ObjectId.Hex()
	err = session.Save(r, w)
	if err != nil {
		log.Print("session")
		log.Print(err)
		controllers.ResponseTemplate(w, responseData, "admin/forms/login")
	}
	http.Redirect(w, r, "/admin/", http.StatusSeeOther)
}

var LogoutAction = func(w http.ResponseWriter, r *http.Request) {
	session, err := getSessionStorage(r)
	if err != nil {
		log.Print(err)
		controllers.ResponseTemplate(w, map[string]interface{}{}, "admin/forms/login")
	}
	session.Values["user_id"] = nil
	err = session.Save(r, w)
	http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
}
