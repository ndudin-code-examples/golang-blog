package admin

import (
	"blog_app/controllers"
	"blog_app/models"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
	"strings"
)

var AuthorsPage = func(w http.ResponseWriter, r *http.Request) {
	admin := models.Author{}
	admins, err := admin.FindAll(map[string]interface{}{"is_deleted": false})
	if err != nil {
		log.Print(err)
		//TODO return error page
	}
	var items []map[string]interface{}
	for _, v := range admins {
		g, _ := Router.Get("admin_admins_get_admin").URL("id", v.ObjectId.Hex())
		d, _ := Router.Get("admin_admins_del_admin").URL("id", v.ObjectId.Hex())
		item := map[string]interface{}{
			"get_url": g.Path,
			"del_url": d.Path,
			"user":    v,
		}
		items = append(items, item)
	}
	responseData := getStartResponseData(w, r)
	responseData["items"] = items
	createUrl, _ := Router.Get("admin_admins_get_admin").URL("id", "create")
	responseData["create_url"] = createUrl.Path
	controllers.ResponseTemplate(w, responseData, "admin/grids/admins")
}

var AuthorPage = func(w http.ResponseWriter, r *http.Request) {
	responseData := getStartResponseData(w, r)
	admin := models.Author{}
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	admin, err := admin.FindFirst(map[string]interface{}{"_id": id})
	if err != nil {
		ui, _ := Router.Get("admin_admins_post_admin").URL()
		responseData["action_url"] = ui.Path
	} else {
		ui, _ := Router.Get("admin_admins_put_admin").URL("id", admin.ObjectId.Hex())
		responseData["action_url"] = ui.Path
	}
	ui, _ := Router.Get("admin_admins_del_admin").URL("id", admin.ObjectId.Hex())
	responseData["delete_url"] = ui.Path
	responseData["item"] = admin
	controllers.ResponseTemplate(w, responseData, "admin/form/admin")
}

var AuthorCreateAction = func(w http.ResponseWriter, r *http.Request) {
	e := r.ParseForm()
	if e != nil {
		log.Print(e)
	}
	admin := models.Author{}
	name := strings.Join(r.Form["name"], "")
	if name != "" {
		admin.Name = name
	}
	login := strings.Join(r.Form["login"], "")
	if login != "" {
		admin.Login = login
	}
	password := strings.Join(r.Form["password"], "")
	if password != "" {
		newPass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		admin.Password = string(newPass)
	}
	id, err := admin.Create()
	if err != nil {
		log.Print(err)
		//TODO handle page
		return
	}
	ui, _ := Router.Get("admin_admins_get_admin").URL("id", id.Hex())
	http.Redirect(w, r, ui.Path, http.StatusSeeOther)
}

var AuthorUpdateAction = func(w http.ResponseWriter, r *http.Request) {
	responseData := getStartResponseData(w, r)
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	admin := models.Author{}
	admin, err := admin.FindFirst(map[string]interface{}{"_id": id})
	if err != nil {
		controllers.ResponseTemplate(w, responseData, "admin/grids/admins")
	}
	e := r.ParseForm()
	if e != nil {
		log.Print(e)
	}
	name := strings.Join(r.Form["name"], "")
	if name != "" {
		admin.Name = name
	}
	login := strings.Join(r.Form["login"], "")
	if login != "" {
		admin.Login = login
	}
	password := strings.Join(r.Form["password"], "")
	if password != "" {
		newPass, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		admin.Password = string(newPass)
	}
	admin.Update()
	ui, err := Router.Get("admin_admins_get_admin").URL("id", vars["id"])
	if err != nil {
		log.Print(err)
	}
	http.Redirect(w, r, ui.Path, http.StatusSeeOther)
}

var AuthorDeleteAction = func(w http.ResponseWriter, r *http.Request) {
	responseData := getStartResponseData(w, r)
	vars := mux.Vars(r)
	id, _ := primitive.ObjectIDFromHex(vars["id"])
	admin := models.Author{}
	admin, err := admin.FindFirst(map[string]interface{}{"_id": id})
	if err != nil {
		controllers.ResponseTemplate(w, responseData, "admin/grids/admins")
	}
	admin.Delete()
	ui, _ := Router.Get("admin_admins").URL("id", vars["id"])
	http.Redirect(w, r, ui.Path, http.StatusSeeOther)
}
