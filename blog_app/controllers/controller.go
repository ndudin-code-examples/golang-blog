package controllers

import (
	"blog_app/models"
	"html/template"
	"log"
	"net/http"
	"strings"
)

type RecourseControllerInterface interface {
	Create() func(w http.ResponseWriter, r *http.Request)
	Read() func(w http.ResponseWriter, r *http.Request)
	ReadAll() func(w http.ResponseWriter, r *http.Request)
	Update() func(w http.ResponseWriter, r *http.Request)
	Delete() func(w http.ResponseWriter, r *http.Request)
	GetRecourseName() string
	SetRecourse(recourse models.RecourseInterface)
	GetRecourse() models.RecourseInterface
}

func ResponseTemplate(w http.ResponseWriter, data map[string]interface{}, templateName string) {
	view, _ := template.ParseGlob("templates/*/*/*.html")
	templateName = getTemplate(templateName)
	err := view.ExecuteTemplate(w, templateName, data)
	if err != nil {
		log.Fatal(err)
	}
}

func ReturnError(w http.ResponseWriter, data map[string]interface{}, subRoute string, errorCode string) {
	ResponseTemplate(w, data, strings.Join([]string{subRoute, "error", errorCode}, "_"))
}

func getTemplate(shortName string) string {
	return strings.ReplaceAll(shortName, "/", "_")
}
