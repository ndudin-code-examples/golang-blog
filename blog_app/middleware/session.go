package middleware

import (
	"blog_app/models"
	"github.com/gorilla/sessions"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"os"
)

func AdminSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		notAuth := []string{"/admin/login"}
		requestPath := r.URL.Path
		for _, value := range notAuth {
			if value == requestPath {
				next.ServeHTTP(w, r)
				return
			}
		}
		SessionKey := os.Getenv("SECRET")
		SessionName := os.Getenv("ADMIN_SESSION_NAME")
		var store = sessions.NewCookieStore([]byte(SessionKey))
		session, err := store.Get(r, SessionName)
		if err != nil {
			log.Print(err)
			http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
		}
		if session.Values["user_id"] == nil {
			http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
		}
		var oid string
		if userId, ok := session.Values["user_id"].(string); ok {
			oid = userId
		} else {
			http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
		}
		var admin = models.Admin{}
		objId, _ := primitive.ObjectIDFromHex(oid)
		admin, err = admin.FindFirst(map[string]interface{}{"_id": objId})
		if err != nil {
			log.Print(err)
			http.Redirect(w, r, "/admin/login", http.StatusSeeOther)
		}
		next.ServeHTTP(w, r)
	})
}

func PortalSession(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r)
	})
}
