package middleware

import (
	"fmt"
	"log"
	"net/http"
)

func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println(fmt.Sprintf("URL: %s - METHOD: %s - ADDR: %s - USER_AGENT: %s", r.URL.RequestURI(), r.Method, r.RemoteAddr, r.UserAgent()))
		next.ServeHTTP(w, r)
	})
}
