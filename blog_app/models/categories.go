package models

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Category struct {
	ObjectId    primitive.ObjectID `bson:"_id,omitempty"`
	Name        string
	Title       string
	Description string
	IsActive    bool `bson:"is_active"`
	Model
}

func (category *Category) ValidateCreate() bool {
	if !category.Validate() {
		return false
	}
	if _, err := category.FindFirst(map[string]interface{}{"name": category.Name}); err == nil {
		return false
	}
	return true
}

func (category *Category) Validate() bool {
	if len(category.Name) == 0 {
		return false
	}
	if len(category.Title) == 0 {
		return false
	}
	if len(category.Description) == 0 {
		return false
	}
	return true
}

func (category *Category) Create() (*primitive.ObjectID, error) {
	if !category.ValidateCreate() {
		return nil, errors.New("data invalid")
	}
	mutator := map[string]interface{}{
		"name":        category.Name,
		"title":       category.Title,
		"description": category.Description,
		"is_active":   true,
		"is_deleted":  false,
	}
	item, err := category.Model.create(mutator, CategoriesCollection)
	return item, err
}

func (category *Category) FindFirst(simpleQuery map[string]interface{}) (Category, error) {
	query := category.Model.simpleQuery(simpleQuery)
	var record Category
	err := category.Model.db(CategoriesCollection).FindOne(context.TODO(), query).Decode(&record)
	return record, err
}

func (category *Category) FindAll(simpleQuery map[string]interface{}) ([]*Category, error) {
	var categories []*Category
	query := category.Model.simpleQuery(simpleQuery)
	cursor, err := category.Model.db(CategoriesCollection).Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	for cursor.Next(context.TODO()) {
		var elem Category
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}
		categories = append(categories, &elem)
	}
	if err := cursor.Err(); err != nil {
		return categories, err
	}
	_ = cursor.Close(context.TODO())
	return categories, nil
}

func (category *Category) Delete() bool {
	return category.Model.delete(map[string]interface{}{"_id": category.ObjectId}, CategoriesCollection)
}

func (category *Category) Update() (bool, error) {
	if !category.Validate() {
		return false, errors.New("invalid data")
	}
	mutator := primitive.M{"name": category.Name, "login": category.Title, "password": category.Description, "is_deleted": category.IsDeleted, "is_active": category.IsActive}
	return category.Model.update(primitive.M{"_id": category.ObjectId}, mutator, CategoriesCollection), nil
}
