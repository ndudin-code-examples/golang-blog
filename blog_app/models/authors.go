package models

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

type Author struct {
	ObjectId primitive.ObjectID `bson:"_id,omitempty"`
	Name     string
	Login    string
	Password string
	Model
}

func (author *Author) ValidateCreate() bool {
	if !author.Validate() {
		return false
	}
	if _, err := author.FindFirst(map[string]interface{}{"login": author.Login}); err == nil {
		return false
	}
	return true
}

func (author *Author) Validate() bool {
	if len(author.Login) == 0 {
		return false
	}
	if len(author.Password) == 0 {
		return false
	}
	return true
}

func (author *Author) Create() (*primitive.ObjectID, error) {
	if !author.ValidateCreate() {
		return nil, errors.New("data invalid")
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(author.Password), bcrypt.DefaultCost)
	mutator := map[string]interface{}{
		"name":       author.Name,
		"login":      author.Login,
		"password":   string(hashedPassword),
		"is_deleted": false,
	}
	item, err := author.Model.create(mutator, AuthorsCollection)
	return item, err
}

func (author *Author) FindFirst(simpleQuery map[string]interface{}) (Author, error) {
	query := author.Model.simpleQuery(simpleQuery)
	var record Author
	err := author.Model.db(AuthorsCollection).FindOne(context.TODO(), query).Decode(&record)
	return record, err
}

func (author *Author) FindAll(simpleQuery map[string]interface{}) ([]*Author, error) {
	var authors []*Author
	query := author.Model.simpleQuery(simpleQuery)
	cursor, err := author.Model.db(AuthorsCollection).Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	for cursor.Next(context.TODO()) {
		var elem Author
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}
		authors = append(authors, &elem)
	}
	if err := cursor.Err(); err != nil {
		return authors, err
	}
	_ = cursor.Close(context.TODO())
	return authors, nil
}

func (author *Author) Delete() bool {
	return author.Model.delete(map[string]interface{}{"_id": author.ObjectId}, AuthorsCollection)
}

func (author *Author) Update() (bool, error) {
	if !author.Validate() {
		return false, errors.New("invalid data")
	}
	mutator := primitive.M{"name": author.Name, "login": author.Login, "password": author.Password, "is_deleted": author.IsDeleted}
	return author.Model.update(primitive.M{"_id": author.ObjectId}, mutator, AuthorsCollection), nil
}
