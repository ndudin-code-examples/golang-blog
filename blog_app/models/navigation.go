package models

import (
	"github.com/gorilla/mux"
	"log"
)

type Navigation struct {
	Items []NavigationItem
}

type NavigationItem struct {
	Name  string
	Route string
	Icon  string
}

func AdminNavigation(router *mux.Router) Navigation {
	var items []NavigationItem

	items = append(items, NavigationItem{Name: "Dashboard", Route: "admin_home", Icon: "mdi-home"})
	items = append(items, NavigationItem{Name: "Admins", Route: "admin_admins", Icon: "mdi-account-key"})
	items = append(items, NavigationItem{Name: "Authors", Route: "admin_authors", Icon: "mdi-account-multiple"})
	items = append(items, NavigationItem{Name: "Articles", Route: "admin_article_read_all", Icon: "mdi-file-document"})
	items = append(items, NavigationItem{Name: "Categories", Route: "admin_categories", Icon: "mdi-filter-variant"})
	items = append(items, NavigationItem{Name: "Tags", Route: "admin_tags", Icon: "mdi-tag"})
	items = append(items, NavigationItem{Name: "Settings", Route: "admin_settings", Icon: "mdi-settings"})
	for k, v := range items {
		route, err := router.Get(v.Route).URL()
		if err != nil {
			log.Print(err)
		}
		items[k].Route = route.Path
	}
	return Navigation{items}
}
