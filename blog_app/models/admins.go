package models

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

type Admin struct {
	ObjectId primitive.ObjectID `bson:"_id,omitempty"`
	Name     string
	Login    string
	Password string
	Model
}

func (admin *Admin) ValidateCreate() bool {
	if !admin.Validate() {
		return false
	}
	if _, err := admin.FindFirst(map[string]interface{}{"login": admin.Login}); err == nil {
		return false
	}
	return true
}

func (admin *Admin) Validate() bool {
	if len(admin.Login) == 0 {
		return false
	}
	if len(admin.Password) == 0 {
		return false
	}
	return true
}

func (admin *Admin) Create() (*primitive.ObjectID, error) {
	if !admin.ValidateCreate() {
		return nil, errors.New("data invalid")
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(admin.Password), bcrypt.DefaultCost)
	mutator := map[string]interface{}{
		"name":       admin.Name,
		"login":      admin.Login,
		"password":   string(hashedPassword),
		"is_deleted": false,
	}
	item, err := admin.Model.create(mutator, AdminsCollection)
	return item, err
}

func (admin *Admin) FindFirst(simpleQuery map[string]interface{}) (Admin, error) {
	query := admin.Model.simpleQuery(simpleQuery)
	var record Admin
	err := admin.Model.db(AdminsCollection).FindOne(context.TODO(), query).Decode(&record)
	return record, err
}

func (admin *Admin) FindAll(simpleQuery map[string]interface{}) ([]*Admin, error) {
	var admins []*Admin
	query := admin.Model.simpleQuery(simpleQuery)
	cursor, err := admin.Model.db(AdminsCollection).Find(context.TODO(), query)
	if err != nil {
		return nil, err
	}
	for cursor.Next(context.TODO()) {
		var elem Admin
		err := cursor.Decode(&elem)
		if err != nil {
			return nil, err
		}
		admins = append(admins, &elem)
	}
	if err := cursor.Err(); err != nil {
		return admins, err
	}
	_ = cursor.Close(context.TODO())
	return admins, nil
}

func (admin *Admin) Delete() bool {
	return admin.Model.delete(map[string]interface{}{"_id": admin.ObjectId}, AdminsCollection)
}

func (admin *Admin) Update() (bool, error) {
	if !admin.Validate() {
		return false, errors.New("invalid data")
	}
	mutator := primitive.M{"name": admin.Name, "login": admin.Login, "password": admin.Password, "is_deleted": admin.IsDeleted}
	return admin.Model.update(primitive.M{"_id": admin.ObjectId}, mutator, AdminsCollection), nil
}
