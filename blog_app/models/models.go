package models

import (
	"blog_app/db"
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"time"
)

const AdminsCollection string = "admins"
const AuthorsCollection string = "authors"
const ArticlesCollection string = "articles"
const CategoriesCollection string = "categories"
const CommentsCollection string = "comments"
const TagsCollection string = "tags"
const SettingsCollection string = "project_settings"

type RecourseInterface interface {
	Create() (primitive.ObjectID, error)
	FindFirst(query map[string]interface{}) (RecourseInterface, error)
	FindAll(query map[string]interface{}) ([]RecourseInterface, error)
	Update() (RecourseInterface, error)
	Delete() (RecourseInterface, error)
	GetId() primitive.ObjectID
}

type Recourse struct {
	RecourseInterface
	Model
}

type Model struct {
	Collection string
	IsDeleted  bool      `bson:"is_deleted"`
	CreateTime time.Time `bson:"created_at,omitempty"`
	UpdateTime time.Time `bson:"updated_at,omitempty"`
}

func (model *Model) create(simpleMutator map[string]interface{}, collection string) (*primitive.ObjectID, error) {
	simpleMutator["created_at"] = time.Now()
	mutator := model.simpleMutator(simpleMutator)
	res, err := model.db(collection).InsertOne(context.TODO(), mutator)
	if err != nil {
		return nil, err
	}
	id := res.InsertedID.(primitive.ObjectID)
	return &id, nil
}

func (model *Model) delete(simpleQuery map[string]interface{}, collection string) bool {
	return model.update(model.simpleQuery(simpleQuery), model.simpleMutator(map[string]interface{}{"is_deleted": true}), collection)
}

func (model *Model) update(query primitive.M, mutator primitive.M, collection string) bool {
	mutator["updated_at"] = time.Now()
	update := primitive.M{"$set": mutator}
	_, err := model.db(collection).UpdateOne(context.TODO(), query, update)
	if err != nil {
		log.Print(err)
		return false
	}
	return true
}

func (model *Model) getCollection(collection string) *mongo.Collection {
	return db.GetDb().Collection(collection)
}

func (model *Model) simpleQuery(query map[string]interface{}) primitive.M {
	filter := primitive.M{}
	for k, v := range query {
		filter[k] = v
	}
	return filter
}

func (model *Model) simpleMutator(query map[string]interface{}) primitive.M {
	mutator := primitive.M{}
	for k, v := range query {
		mutator[k] = v
	}
	return mutator
}

func (model *Model) db(collection string) *mongo.Collection {
	return model.getCollection(collection)
}
