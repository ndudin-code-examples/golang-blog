package models

import "go.mongodb.org/mongo-driver/bson/primitive"

func GetArticleRecourse() *ArticleRecourse {
	return &ArticleRecourse{}
}

type ArticleRecourse struct {
	ObjectId primitive.ObjectID `bson:"_id,omitempty"`
	Title    string
	Uri      string
	Preview  string
	Body     string
	AuthorId primitive.ObjectID `bson:"authors_id"`
	IsActive bool               `bson:"is_active"`
	Recourse
}

func (model *ArticleRecourse) Create() (primitive.ObjectID, error) {
	oid := primitive.NewObjectID()
	return oid, nil
}
func (model *ArticleRecourse) FindFirst(query map[string]interface{}) (RecourseInterface, error) {
	var item RecourseInterface
	return item, nil
}
func (model *ArticleRecourse) FindAll(query map[string]interface{}) ([]RecourseInterface, error) {
	var items []RecourseInterface
	return items, nil
}
func (model *ArticleRecourse) Update() (RecourseInterface, error) {
	var item RecourseInterface
	return item, nil
}
func (model *ArticleRecourse) Delete() (RecourseInterface, error) {
	var item RecourseInterface
	return item, nil
}
func (model *ArticleRecourse) GetId() primitive.ObjectID {
	return model.ObjectId
}
