package routing

import (
	"blog_app/controllers"
	"blog_app/controllers/admin"
	"blog_app/middleware"
	"github.com/gorilla/mux"
	"log"
	"strings"
)

type Routing struct {
	Router *mux.Router
}

func (routing *Routing) GetRoute(subRoute string, path string, method string) (*string, error) {
	route, err := routing.Router.Get(routeName(subRoute, path, method)).URL()
	if err != nil {
		return nil, err
	}
	return &route.Path, nil
}

func GetRouting() Routing {
	router := mux.NewRouter()
	adminRouting(router)

	//cabinetRoute := router.PathPrefix("/cabinet").Subrouter()
	//cabinetRoute.HandleFunc("/", admin.DashboardPage).Methods("GET").Name("cabinet_home")
	//cabinetRoute.HandleFunc("/login", admin.LoginPage).Methods("GET").Name("cabinet_login_page")
	//cabinetRoute.HandleFunc("/login", admin.LoginAction).Methods("POST").Name("cabinet_login_action")

	//router.HandleFunc("/api/user/new", controllers.CreateAccount).Methods("POST")
	//router.HandleFunc("/api/user/login", controllers.Authenticate).Methods("POST")
	//router.HandleFunc("/api/user/posts", controllers.GetUserPosts).Methods("GET")
	//router.HandleFunc("/api/posts", controllers.GetPosts).Methods("GET")

	//router.HandleFunc("/api/posts", controllers.CreatePost).Methods("POST")
	//router.HandleFunc("/api/posts/{id}", controllers.GetPost).Methods("GET")
	//router.HandleFunc("/api/posts/{id}", controllers.DeletePost).Methods("DELETE")
	//router.HandleFunc("/api/posts/{id}", controllers.UpdatePost).Methods("PUT")

	//router.Use(middleware.Logger, middleware.PortalSession, middleware.AdminSession)
	//router.NotFoundHandler = http.HandlerFunc(controllers.NotFound)
	admin.Router = router
	return Routing{router}
}

func adminRouting(router *mux.Router) {
	adminRoute := router.PathPrefix("/admin").Subrouter()
	adminRoute.HandleFunc("/", admin.DashboardPage).Methods("GET").Name("admin_home")
	adminRoute.HandleFunc("/login", admin.LoginPage).Methods("GET").Name("admin_login_page")
	adminRoute.HandleFunc("/login", admin.LoginAction).Methods("POST").Name("admin_login_action")
	adminRoute.HandleFunc("/logout", admin.LogoutAction).Methods("GET").Name("admin_logout")

	adminAdminsRouter := adminRoute.PathPrefix("/admins").Subrouter()
	adminAdminsRouter.HandleFunc("/", admin.AdminsPage).Methods("GET").Name("admin_admins")
	adminAdminsRouter.HandleFunc("/{id}", admin.AdminPage).Methods("GET").Name("admin_admins_get_admin")
	adminAdminsRouter.HandleFunc("/create", admin.AdminCreateAction).Methods("POST").Name("admin_admins_post_admin")
	adminAdminsRouter.HandleFunc("/{id}/update", admin.AdminUpdateAction).Methods("POST").Name("admin_admins_put_admin")
	adminAdminsRouter.HandleFunc("/{id}/delete", admin.AdminDeleteAction).Methods("POST").Name("admin_admins_del_admin")

	adminAuthorsRouter := adminRoute.PathPrefix("/authors").Subrouter()
	adminAuthorsRouter.HandleFunc("/", admin.AuthorsPage).Methods("GET").Name("admin_authors")
	adminAuthorsRouter.HandleFunc("/{id}", admin.AuthorPage).Methods("GET").Name("admin_authors_get_admin")
	adminAuthorsRouter.HandleFunc("/create", admin.AuthorCreateAction).Methods("POST").Name("admin_authors_post_admin")
	adminAuthorsRouter.HandleFunc("/{id}/update", admin.AuthorUpdateAction).Methods("POST").Name("admin_authors_put_admin")
	adminAuthorsRouter.HandleFunc("/{id}/delete", admin.AuthorDeleteAction).Methods("POST").Name("admin_authors_del_admin")

	Recourse(adminRoute, "admin", admin.GetArticleController())
	//Recourse(adminRoute,"admin", admin.GetArticleController())
	//Recourse(adminRoute,"admin", admin.GetArticleController())
	//Recourse(adminRoute,"admin", admin.GetArticleController())
	//Recourse(adminRoute,"admin", admin.GetArticleController())
	//Recourse(adminRoute,"admin", admin.GetArticleController())

	/*adminArticlesRouter := adminRoute.PathPrefix("/articles").Subrouter()
	adminArticlesRouter.HandleFunc("/", admin.AdminsPage).Methods("GET").Name("admin_articles")
	adminArticlesRouter.HandleFunc("/{id}", admin.AdminPage).Methods("GET").Name("admin_articles_get_admin")
	adminArticlesRouter.HandleFunc("/create", admin.AdminCreateAction).Methods("POST").Name("admin_articles_post_admin")
	adminArticlesRouter.HandleFunc("/{id}/update", admin.AdminUpdateAction).Methods("POST").Name("admin_articles_put_admin")
	adminArticlesRouter.HandleFunc("/{id}/delete", admin.AdminDeleteAction).Methods("POST").Name("admin_articles_del_admin")*/

	adminCategoriesRouter := adminRoute.PathPrefix("/categories").Subrouter()
	adminCategoriesRouter.HandleFunc("/", admin.AdminsPage).Methods("GET").Name("admin_categories")
	adminCategoriesRouter.HandleFunc("/{id}", admin.AdminPage).Methods("GET").Name("admin_categories_get_admin")
	adminCategoriesRouter.HandleFunc("/create", admin.AdminCreateAction).Methods("POST").Name("admin_categories_post_admin")
	adminCategoriesRouter.HandleFunc("/{id}/update", admin.AdminUpdateAction).Methods("POST").Name("admin_categories_put_admin")
	adminCategoriesRouter.HandleFunc("/{id}/delete", admin.AdminDeleteAction).Methods("POST").Name("admin_categories_del_admin")

	adminTagsRouter := adminRoute.PathPrefix("/tags").Subrouter()
	adminTagsRouter.HandleFunc("/", admin.AdminsPage).Methods("GET").Name("admin_tags")
	adminTagsRouter.HandleFunc("/{id}", admin.AdminPage).Methods("GET").Name("admin_tags_get_admin")
	adminTagsRouter.HandleFunc("/create", admin.AdminCreateAction).Methods("POST").Name("admin_tags_post_admin")
	adminTagsRouter.HandleFunc("/{id}/update", admin.AdminUpdateAction).Methods("POST").Name("admin_tags_put_admin")
	adminTagsRouter.HandleFunc("/{id}/delete", admin.AdminDeleteAction).Methods("POST").Name("admin_tags_del_admin")

	adminSettingsRouter := adminRoute.PathPrefix("/settings").Subrouter()
	adminSettingsRouter.HandleFunc("/", admin.AdminsPage).Methods("GET").Name("admin_settings")
	adminSettingsRouter.HandleFunc("/{id}", admin.AdminPage).Methods("GET").Name("admin_settings_get_admin")
	adminSettingsRouter.HandleFunc("/create", admin.AdminCreateAction).Methods("POST").Name("admin_settings_post_admin")
	adminSettingsRouter.HandleFunc("/{id}/update", admin.AdminUpdateAction).Methods("POST").Name("admin_settings_put_admin")
	adminSettingsRouter.HandleFunc("/{id}/delete", admin.AdminDeleteAction).Methods("POST").Name("admin_settings_del_admin")

	adminRoute.Use(middleware.Logger, middleware.AdminSession)
}

func Recourse(router *mux.Router, subRoute string, controller controllers.RecourseControllerInterface) {
	recourseName := controller.GetRecourseName()
	recourseRouter := router.PathPrefix("/" + recourseName).Subrouter()
	recourseRouter.HandleFunc("/", controller.ReadAll()).Methods("GET").Name(routeName(subRoute, recourseName, "read_all"))
	recourseRouter.HandleFunc("/{id}", controller.Read()).Methods("GET").Name(routeName(subRoute, recourseName, "read"))
	recourseRouter.HandleFunc("/create", controller.Create()).Methods("POST").Name(routeName(subRoute, recourseName, "create"))
	recourseRouter.HandleFunc("/{id}/update", controller.Update()).Methods("POST").Name(routeName(subRoute, recourseName, "update"))
	recourseRouter.HandleFunc("/{id}/delete", controller.Delete()).Methods("POST").Name(routeName(subRoute, recourseName, "delete"))
}

func routeName(subRoute string, path string, method string) string {
	s := strings.Join([]string{subRoute, path, method}, "_")
	log.Print(s)
	return s
}
