FROM golang:1.12
LABEL maintainer="zim"
ENV GOPATH /go
WORKDIR ${GOPATH}/src/blog_app
COPY ./blog_app .
RUN go get -d -v ./...
RUN go install -v ./...
CMD ["/go/bin/blog_app"]